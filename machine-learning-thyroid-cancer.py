import numpy as np
import pandas as pd
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, roc_auc_score, confusion_matrix
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.naive_bayes import GaussianNB
SEED = 5

# https://zenodo.org/record/6465436
thyroid_table = pd.read_csv("thyroid_clean.csv", index_col=0)

# ensure equal number of samples per class by including all of 1 class and discarding the other
thyroid_mal_0 = thyroid_table[thyroid_table["mal"] == 0]
thyroid_mal_1 = thyroid_table[thyroid_table["mal"] == 1]

n = min(len(thyroid_mal_0), len(thyroid_mal_1))

thyroid_mal_0 = thyroid_mal_0.sample(n, random_state=SEED)
thyroid_mal_1 = thyroid_mal_1.sample(n, random_state=SEED)

# new table
thyroid_table_np = pd.concat([thyroid_mal_0, thyroid_mal_1])

# don't include labels in training data
# labels should be a 1D array
thyroid_labels = thyroid_table_np["mal"]
thyroid_table_np = thyroid_table_np.drop("mal", axis=1)

X_train, X_test, y_train, y_test = train_test_split(thyroid_table_np, thyroid_labels, test_size = 0.2, random_state=SEED)

results = pd.DataFrame(columns=["Model", "Accuracy", "AUC", "FPR", "FNR"])

def evaluate_model_help(model):
  # normalize the data before training
  pipe = make_pipeline(StandardScaler(), model)
  pipe.fit(X_train, y_train)
  y_pred = pipe.predict(X_test)

  acc = accuracy_score(y_test, y_pred) * 100
  auc = roc_auc_score(y_test, pipe.predict_proba(X_test)[:,1])

  confused_matrix = confusion_matrix(y_test, y_pred)
  TN = confused_matrix[0][0]
  FN = confused_matrix[1][0]
  TP = confused_matrix[1][1]
  FP = confused_matrix[0][1]
  FPR = FP/(FP+TN)
  FNR = FN/(TP+FN)

  return [acc, auc, FPR, FNR]

def evaluate_model(model, name):
  res = evaluate_model_help(model)
  res.insert(0, name)
  results.loc[len(results)] = res

evaluate_model(RandomForestClassifier(random_state=SEED), "Random Forest")
evaluate_model(GradientBoostingClassifier(random_state=SEED), "GBM")
evaluate_model(SVC(kernel="linear", random_state=SEED, probability=True), "SVM (linear)")
evaluate_model(SVC(kernel="rbf", random_state=SEED, probability=True), "SVM (radial)")
evaluate_model(LogisticRegression(random_state=SEED), "Logistic")
evaluate_model(LinearDiscriminantAnalysis(), "LDA")
evaluate_model(GaussianNB(), "Naive Bayes")

print(results.round(3).to_markdown(index=False))
